'use strict';

describe('Component: MypollsComponent', function () {

  // load the controller's module
  beforeEach(module('dashboardApp'));

  var MypollsComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    MypollsComponent = $componentController('MypollsComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
