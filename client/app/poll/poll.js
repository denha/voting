'use strict';

angular.module('dashboardApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('poll', {
        url: '/poll',
        template: '<poll></poll>'
      });
    $stateProvider.state('polldetail', {
      url: '/poll/:id',
      templateUrl: 'app/poll/polldetail.html',
      controller: 'PollDetailController',
      controllerAs: 'p'
    });
  });
