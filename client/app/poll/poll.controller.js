'use strict';
(function () {

  class PollComponent {
    constructor($http, $location, Auth) {
      this.Auth = Auth;
      this.$http = $http;
      this.$location = $location;
      this.options = [{value: undefined}, {value: undefined}];
      this.optionsValid = true;
      this.titleValid = true;
    }

    newOption() {
      console.log(this.options)
      this.options.push({value: undefined});
    }

    removeOption(index) {
      this.options.splice(index, 1);
    }

    submitPoll() {
      this.titleValid = typeof this.title !== 'undefined';
      this.optionsValid = this.options.every(function (element) {
        return typeof element.value !== 'undefined';
      });
      if (this.titleValid && this.optionsValid) {
        var optionsReduced = this.options.map(function (element) {
          return {_id: element.value, votes: 0};
        });
        var poll = {
          title: this.title,
          user: this.Auth.getCurrentUser().email,
          options: optionsReduced,
          date: new Date
        };
        console.log(poll);
        var $location = this.$location;
        this.$http.post('/api/polls', poll).then(function (res) {
          console.log(res);
          if (res.data._id) {
            $location.path('/poll/' + res.data._id);
          }
        });
      }
    }
  }

  angular.module('dashboardApp')
    .component('poll', {
      templateUrl: 'app/poll/poll.html',
      controller: PollComponent
    });

})();
