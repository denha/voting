'use strict';

import mongoose from 'mongoose';
import Schema from "mongoose/lib/schema";

// var optionSchema = new Schema({_id: String, votes: Number});

var PollSchema = new mongoose.Schema({
  title: String,
  user: String,
  options: [mongoose.Schema.Types.Mixed],
  date: Date
});

export default mongoose.model('Poll', PollSchema);
