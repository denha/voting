'use strict';

(function() {

class MainController {

  constructor($http, $scope, socket, Auth) {
    this.$http = $http;
    this.socket = socket;
    this.awesomeThings = [];
    this.polls = [];
    this.predicate = 'date';
    this.reverse = true;
    Auth.isLoggedIn(function (user) {
      this.user = user;
    }.bind(this));

    $scope.$on('$destroy', function() {
      socket.unsyncUpdates('thing');
    });
  }

  $onInit() {
    this.$http.get('/api/things').then(response => {
      this.awesomeThings = response.data;
      this.socket.syncUpdates('thing', this.awesomeThings);
    });
    this.$http.get('/api/polls').then(response => {
      this.polls = response.data;
    });
  }

  addThing() {
    if (this.newThing) {
      this.$http.post('/api/things', { name: this.newThing });
      this.newThing = '';
    }
  }

  deleteThing(thing) {
    this.$http.delete('/api/things/' + thing._id);
  }

  order(by) {
    this.reverse = (this.predicate === by) ? !this.reverse : false;
    this.predicate = by;
  }
}

angular.module('dashboardApp')
  .component('main', {
    templateUrl: 'app/main/main.html',
    controller: MainController
  });

})();
