

class PollDetailController {
  constructor(Auth, $scope, $stateParams, $http) {
    this.Auth = Auth;
    this.$scope = $scope;
    this.$stateParams = $stateParams;
    this.$http = $http;

    $scope.id = $stateParams.id;
    $http.get('/api/polls/' + $scope.id).then(function (response) {
      // console.log(response.data);
      $scope.poll = response.data;
      $scope.labels = [];
      $scope.data = [];
      $scope.poll.options.forEach(function (value, index, array) {
        $scope.labels.push(value._id);
        $scope.data.push(value.votes);
      })
      $scope.link = window.location.href;
      new Clipboard('.btn');
    });
  }

  vote(index) {
    var $scope = this.$scope;
    var $http = this.$http;
    $scope.poll.options[index].votes = $scope.poll.options[index].votes + 1;
    $http.delete('/api/polls/' + $scope.id).then(function (response) {
      $http.post('/api/polls/', $scope.poll).then(function (response) {
        console.log(response.data)
        $scope.poll = response.data;
        $scope.labels = [];
        $scope.data = [];
        $scope.poll.options.forEach(function (value, index, array) {
          $scope.labels.push(value._id);
          $scope.data.push(value.votes);
        })
      });
    });
  }

  addOption(name) {
    var $scope = this.$scope;
    var $http = this.$http;
    $scope.poll.options.push({_id: name, votes: 0});

    $http.delete('/api/polls/' + $scope.id, $scope.poll).then(function (response) {
      $http.post('/api/polls/', $scope.poll).then(function (response) {
        $scope.poll = response.data;
      });
    });
  }
}

angular.module('dashboardApp')
  .controller('PollDetailController', PollDetailController);
