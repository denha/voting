'use strict';

angular.module('dashboardApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('mypolls', {
        url: '/mypolls',
        template: '<mypolls></mypolls>',
        authenticate: true
      });
  });
