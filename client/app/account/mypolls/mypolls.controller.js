'use strict';
(function(){

class MypollsComponent {
  constructor($http, $scope, Auth) {
    this.$http = $http;
    this.$scope = $scope;
    $scope.user = Auth.getCurrentUser();
    $scope.search = {};
    $scope.search.user = $scope.user.email
    $http.get('/api/polls').then(function (response) {
      console.log(response.data);
      $scope.polls = response.data
    });
  }

  deletePoll(id) {
    var $http = this.$http
    var $scope = this.$scope
    $http.delete('/api/polls/' + id).then(function(response) {
      $http.get('/api/polls').then(function (response) {
        $scope.polls = response.data
      });
    })
  }
}

angular.module('dashboardApp')
  .component('mypolls', {
    templateUrl: 'app/account/mypolls/mypolls.html',
    controller: MypollsComponent
  });

})();
